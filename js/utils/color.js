export var getRandomColor = () => {
  var letters = '0123456789ABCDEF'
  var color = '#'
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)]
  }
  return color
}

export var rgb2hex = (str) =>
  '#' +
  str
    .split(',')
    .map((s) => (s.replace(/\D/g, '') | 0).toString(16))
    .map((s) => (s.length < 2 ? '0' + s : s))
    .join('')

export function verifyColor(element) {
  var arr = [...element]
  arr.map((box) => {
    var bg = window
      .getComputedStyle(box, null)
      .getPropertyValue('background-color')
    var cor = rgb2hex(bg)
    !isLight(cor)
      ? (box.classList.add('light-text'), box.classList.remove('dark-text'))
      : (box.classList.add('dark-text'), box.classList.remove('light-text'))
  })
}

var isLight = (color) => {
  const hex = color.replace('#', '')
  const r = parseInt(hex.substr(0, 2), 16)
  const g = parseInt(hex.substr(2, 2), 16)
  const b = parseInt(hex.substr(4, 2), 16)
  const brightness = (r * 299 + g * 587 + b * 114) / 1000
  return brightness > 155.5
}
