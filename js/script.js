import { getRandomColor, rgb2hex, verifyColor } from './utils/color.js'
const boxs = document.querySelector('.boxs')
const textBox = document.createElement('input')

const getAllElements = (element) => document.querySelectorAll(element)

const styleAlert = (bg) => {
  textBox.value = bg
  document.body.appendChild(textBox)
  textBox.select()
  document.execCommand('copy')
  document.body.removeChild(textBox)
  swal(textBox.value, 'Cor copiada para a área de transferência!', 'success')
  var swalTitle = document.querySelector('.swal-title')
  swalTitle.style.backgroundColor = textBox.value
  swalTitle.style.display = 'flex'
  swalTitle.style.marginLeft = '70px'
  swalTitle.style.marginRight = '70px'
  swalTitle.style.borderRadius = '14px'
  swalTitle.style.padding = '20px'
  swalTitle.style.alignItems = 'center'
  swalTitle.style.justifyContent = 'center'
  swalTitle.style.color = 'white'
  verifyColor(getAllElements('.swal-title'))
}

const createBox = () => {
  var box = document.createElement('div')
  box.setAttribute('id', 'box')
  box.style.backgroundColor = getRandomColor()
  box.append(rgb2hex(box.style.backgroundColor))
  box.onclick = () => styleAlert(rgb2hex(box.style.backgroundColor))
  boxs.appendChild(box)
  verifyColor(getAllElements('#box'))
}

const clearBox = () => {
  var element = getAllElements('#box')
  for (var i = 0; i < element.length; i++) {
    var boxDiv = document.getElementById('box')
    boxDiv.parentNode.removeChild(boxDiv)
  }
}

const createTen = () => {
  for (var i = 0; i < 10; i++) {
    createBox()
  }
}

const clearLast = () => {
  var element = getAllElements('#box')
  var last = element[element.length - 1]
  if (element.length > 0) {
    last.parentNode.removeChild(last)
  }
}

const replaceColors = () => {
  var element = getAllElements('#box')
  for (var i = 0; i < element.length; i++) {
    element[i].style.backgroundColor = getRandomColor()
    element[i].innerHTML = rgb2hex(element[i].style.backgroundColor)
  }
  setTimeout(() => {
    verifyColor(getAllElements('#box'))
  }, 300)
}

window.createBox = createBox
window.clearBox = clearBox
window.createTen = createTen
window.clearLast = clearLast
window.replaceColors = replaceColors

createTen()
