const html = document.querySelector('html')
const checkbox = document.querySelector('input[name=check')
let verifyTheme = localStorage.getItem('theme')

const getStyle = (element, style) =>
  window.getComputedStyle(element).getPropertyValue(style)

const defaultColors = {
  bg: getStyle(html, '--bg'),
  colorText: getStyle(html, '--color-text'),
  fontWeight: getStyle(html, '--font-weight'),
  boxCreate: getStyle(html, '--box-create'),
  boxTen: getStyle(html, '--box-ten'),
  colorHuh: getStyle(html, '--color-huh'),
  boxShadow: getStyle(html, '--box-shadow'),
  filterImg: getStyle(html, '--filter-img'),
}

const darkMode = {
  bg: '#211d1e',
  colorText: '#ffff',
  fontWeight: 'bold',
  boxCreate: '#006afc',
  boxTen: '#00fff3',
  colorHuh: '#ed7190',
  boxShadow: '3px 3px 15px #090909, -3px -3px 15px #151515',
  filterImg: 'brightness(80%) contrast(200%) drop-shadow(0px 0px 2px #ed7190)',
}

const convertKey = (key) => '--' + key.replace(/([A-Z])/, '-$1').toLowerCase()

const changeColors = (colors) => {
  Object.keys(colors).map((key) => {
    html.style.setProperty(convertKey(key), colors[key])
  })
}

if (verifyTheme == 'dark') {
  changeColors(darkMode)
  checkbox.checked = true
}

checkbox.addEventListener('change', ({ target }) => {
  target.checked
    ? (localStorage.setItem('theme', 'dark'), changeColors(darkMode))
    : (localStorage.setItem('theme', null), changeColors(defaultColors))
})
